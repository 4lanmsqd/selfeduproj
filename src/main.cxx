#include "iostream"
#include "vector"

#pragma warning(disable: 26812)

namespace ObjectModel{

enum class Wrapper : int8_t
{
    PRIMITIVE = 1,
    ARRAY,
    STRING,
    OBJECT
};


enum class Type : int8_t
{
    i8 = 1,
    i16,
    i32,
    i64,

    U8,
    U16,
    U32,
    U64,

    FLOAT,
    DOUBLE,


    BOOL
};

class Root
{
    protected:
        std :: string name;
        int16_t nameLength;
        int8_t wrapper;
        int32_t size;
        int8_t Type;
    protected:
        Root();
    public:
        int32_t getSize();
        void setName(std :: string);
        std :: string getName();
        virtual void pack(std :: vector<int8_t>*, int16_t*);
}; 


class Primitive : public Root  
{
    private:
        int8_t type;
        std :: vector<int8_t*> data;
    private:
        Primitive();
    public:
        static Primitive* createI32(std :: string name, ObjectModel::Type type, int32_t value);
}; 


class Array : public Root 
{

}; 


class Object : public Root 
{

};

Root::Root() 
    : 
    name("unknown"),
    wrapper(0),
    nameLength(0), 
    size(sizeof(nameLength) + sizeof(wrapper) + sizeof(size))
{}

// definition 

void Root::setName(std :: string name){
this->name = name;
nameLength = (int16_t)name.size();
name += nameLength;
}

int32_t Root::getSize() 
{
    return size;
}

std :: string Root::getName(){
    return name;
}

Primitive* Primitive::createI32(std::string name, ObjectModel::Type type, int32_t value)
{
    Primitive* p = new Primitive();
    p -> setName(name);
    p -> wrapper = static_cast<int8_t>(Wrapper::PRIMITIVE);
    p -> Type = static_cast<int8_t>(type);
    p -> data = new std :: vector<int8_t>(sizeof value);

    return p;
}

};

namespace EventSystem
{
    class Event;
    class System {
        private:
            friend class Event;
            std :: string name;
            int32_t descriptor;
            int16_t index;
            bool active;
            std :: vector<Event*> events;
        public:
            System(std::string);
            ~System();
            void addEvent(Event*);
            Event* getEvent();
            bool isActive();

    };

    class Event {
    public:
        enum class DeviceType : int8_t
        {
            KEYBOARD = 1,
            MOUSE,
            TOUCHPAD,
            JOYSTICK
        };
        DeviceType dType;
        System* system = nullptr;
    public:
        Event(DeviceType);
        DeviceType getdType();
        friend std::ostream& operator<<(std::iostream& stream, const DeviceType dType){
            using namespace EventSystem;
            std :: string result; 
#define PRINT(a) result = #a;
            switch (dType){
                case DeviceType::KEYBOARD: PRINT(KEYBOARD); break;
                case DeviceType::MOUSE: PRINT(MOUSE); break;
                case DeviceType::TOUCHPAD: PRINT(TOUCHPAD); break;
                case DeviceType::JOYSTICK: PRINT(JOYSTICK); break;
            }
            return stream << result;
        }
        void bind (System*, Event*);

    };

    class KeyboardEvent : public Event{
    private:
        int16_t keyCode;
        bool pressed;
        bool released;
    public:
        KeyboardEvent(int16_t, bool, bool);
    };
    
    System::System(std::string name):
    name(name),
    descriptor(123),
    index(1),
    active(true) {}; System::~System(){}

    void System::addEvent(Event* e){
    e->bind(this, e);
    }

    Event* System::getEvent(){
        return events.front();
    }

    bool System::isActive() {
        if (!system) return false;
        return true;
    }

   Event :: Event (DeviceType dType){
        this->dType = dType;
    }

    void Event::bind(System* system, Event *e){
        this -> system = system;
        this -> system -> events.push_back(e);
    }

    Event::DeviceType Event::getdType(){
        return this->dType;
    }

    KeyboardEvent::KeyboardEvent(int16_t keyCode, bool pressed, bool released) 
        : 
        Event(Event::DeviceType::KEYBOARD),
        keyCode(keyCode),
        pressed(pressed),
        released(released)
         {}
};


using namespace EventSystem;


int main(){
    System Foo("Foo");
    Event* e = new KeyboardEvent('a', true, false);
    Foo.addEvent(e);
    KeyboardEvent* kb = static_cast<KeyboardEvent*>(Foo.getEvent());
}